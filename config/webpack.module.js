const merge = require('webpack-merge')
const common = require('./webpack.common')
const lodash = require('lodash')

const {resolve} = require("./utils");
let argv = require('yargs').argv
let moduleName = argv.module
let ucModuleName = lodash.upperFirst(moduleName)
// const otherLoadName = new RegExp(`$@\/${moduleName}\/\S+$`)

console.log('模块名：', ucModuleName)

module.exports = merge(common, {
  mode: "production",
  entry: resolve(`src/${moduleName}/main.js`),
  output: {
    publicPath: "/manage",
    filename: 'main.js',
    path: resolve(`dist/${moduleName}`),
    chunkFilename: "module.[hash].js",
    library: {
      root: `flick${ucModuleName}`
    },
    libraryTarget: 'umd'
  },
  // externals: [
  //   function (context, request, callback) {
  //     console.log('请求地址',request)
  //     console.log(context)
  //     // if (!(otherLoadName.test(request))) {
  //     //   return callback(null, request);
  //     // }
  //     callback();
  //   }
  // ],
  devServer: {
    // 热重载
    hot: true,
    // 错误显示在页面中
    overlay: true,
    // 信息打印
    stats: "errors-only",
    // 自动打开
  },
  plugins: [
    // new HtmlWebpackPlugin({
    //   template: resolve('public/index.html')
    // })
    // new CopyWebpackPlugin([
    //   {
    //     from: resolve("static"),
    //     to: "static",
    //     ignore: [".*"]
    //   },
    //   // {
    //   //   from: resolve("public/config.js"),
    //   //   to: "config.js"
    //   // },
    //   {
    //     from: resolve("public/favicon.ico"),
    //     to: "favicon.ico"
    //   }
    // ])
  ]
})