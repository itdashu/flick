const merge = require('webpack-merge')
const common = require('./webpack.common')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const WebpackCdnPlugin = require('webpack-cdn-plugin')

const {resolve} = require("./utils");

module.exports = merge(common, {
  mode: "development",
  entry: resolve('src/manage/main.js'),
  output: {
    publicPath: "/manage",
    filename: 'app.js',
    path: resolve('dist')
  },
  devtool: "inline-source-map",
  devServer: {
    // 热重载
    // hot: true,
    host: '127.0.0.1',
    port: '8383',
    // 错误显示在页面中
    overlay: true,
    // 信息打印
    // stats: "errors-only",
    // 自动打开
    open: false,
    // history 模式
    // historyApiFallback: {
    //   index: "src/index.html"
    // }
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: resolve('public/index.html')
    }),
    new WebpackCdnPlugin({
      modules: [
        {
          name: 'vue',
          var: 'Vue',
          path: 'dist/vue.runtime.min.js'
        },
        {
          name: 'vue-router',
          var: 'VueRouter',
          path: 'dist/vue-router.min.js'
        },
        {
          name: 'vuex',
          var: 'Vuex',
          path: 'dist/vuex.min.js'
        },
        {
          name: 'view-design',
          var: 'iview',
          path: 'dist/iview.min.js'
        }
      ],
      publicPath: '/node_modules'
    })
    // new CopyWebpackPlugin([
    //   {
    //     from: resolve("static"),
    //     to: "static",
    //     ignore: [".*"]
    //   },
    //   // {
    //   //   from: resolve("public/config.js"),
    //   //   to: "config.js"
    //   // },
    //   {
    //     from: resolve("public/favicon.ico"),
    //     to: "favicon.ico"
    //   }
    // ])
  ]
})