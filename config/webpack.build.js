const merge = require('webpack-merge')
const common = require('./webpack.common')
const WebpackCdnPlugin = require('webpack-cdn-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')

const { resolve } = require("./utils");

module.exports = merge(common,{
  mode: "production",
  entry: resolve('src/manage/main.js'),
  output: {
    publicPath: "/manage",
    filename:'app.js',
    path: resolve('dist/manage'),
    chunkFilename: "app.[hash].js",
    library: 'manage',
    libraryTarget: 'umd'
  },
  devServer: {
    // 热重载
    hot: true,
    // 错误显示在页面中
    overlay: true,
    // 信息打印
    stats: "errors-only",
    // 自动打开
    open: false,
    // history 模式
    // historyApiFallback: {
    //   index: "src/index.html"
    // }
  },
  devtool: "inline-source-map",
  plugins:[
    new HtmlWebpackPlugin({
      template: resolve('public/index.html')
    }),
    new WebpackCdnPlugin({
      modules: [
        {
          name: 'vue',
          var: 'Vue',
          path: 'dist/vue.runtime.min.js'
        },
        {
          name: 'vue-router',
          var: 'VueRouter',
          path: 'dist/vue-router.min.js'
        },
        {
          name: 'vuex',
          var: 'Vuex',
          path: 'dist/vuex.min.js'
        },
        {
          name: 'lodash',
          var: 'lodash',
          path: 'lodash.min.js'
        }
      ],
      publicPath: '/node_modules'
    })
    // new CopyWebpackPlugin([
    //   {
    //     from: resolve("static"),
    //     to: "static",
    //     ignore: [".*"]
    //   },
    //   // {
    //   //   from: resolve("public/config.js"),
    //   //   to: "config.js"
    //   // },
    //   {
    //     from: resolve("public/favicon.ico"),
    //     to: "favicon.ico"
    //   }
    // ])
  ]
})