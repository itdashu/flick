const List = () => import(/* webpackChunkName: "user" */ '../view/List')
const Role = () => import(/* webpackChunkName: "user" */ '../view/Role')
const Config = () => import(/* webpackChunkName: "user" */ '../view/Config')
// import Index from "../view/Index";
// import Role from "../view/Role";

export default [
  {
    path: '/user/list',
    name: 'userList',
    component: List
  },
  {
    path: '/user/role',
    name: 'userRole',
    component: Role
  },
  {
    path: '/user/config',
    name: 'userConfig',
    component: Config
  }
]