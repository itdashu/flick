import Vue from "vue";
import Vuex from "vuex";
import {localStorage as storage} from 'lsy-storages'

const currentMainMenuIndex = storage.getItem('store:manage:currentMainMenuIndex')
const subMenuIndex = storage.getItem('store:manage:subMenuIndex')
const subOpenIndex = storage.getItem('store:manage:subOpenIndex')

Vue.use(Vuex);
const store = new Vuex.Store({
  modules: {
    manage: {
      namespaced: true,
      state: {
        user: {},
        menus: [
          {
            name: '概况',
            link: '/',
            icon: '//cdn.itdashu.com/icon/home.svg'
          },
          {
            name: '用户管理',
            link: '/user',
            icon: '//cdn.itdashu.com/icon/user.svg',
            children: [
              {
                name: '用户列表',
                link: '/user/list'
              },
              {
                name: '角色管理',
                link: '/user/role'
              },
              {
                name: '模块设置',
                link: '/user/config'
              },
            ]
          }
        ],
        currentMainMenuIndex: currentMainMenuIndex ? currentMainMenuIndex : false,
        subMenuIndex: subMenuIndex ? subMenuIndex : false,
        subOpenIndex: subOpenIndex ? subOpenIndex : {},
        menuLayout: '',
        menuOverPosition: 0,
        menuOverDisplay: false,
        channel: {
          name: '',
          data: {}
        }
      },
      mutations: {
        SET_MENUS: (state, menus) => (state.menus = menus),
        SET_CURRENT_MAIN_MENU_INDEX: (state, index) => {
          state.currentMainMenuIndex = index
          storage.setItem('store:manage:currentMainMenuIndex', index)
        },
        SET_SUB_MENU_INDEX: (state, index) => {
          state.subMenuIndex = index
          storage.setItem('store:manage:subMenuIndex', index)
        },
        SET_SUB_OPEN_INDEX: (state, options) => {
          let subOpenIndex = JSON.parse(JSON.stringify(state.subOpenIndex))
          if (subOpenIndex.hasOwnProperty(options.index)) {
            delete subOpenIndex[options.index]
          } else {
            subOpenIndex[options.index] = true
          }

          state.subOpenIndex = {...subOpenIndex}
          console.log(JSON.parse(JSON.stringify(state.subOpenIndex)))
          storage.setItem('store:manage:subOpenIndex', subOpenIndex)
        },
        SWITCH_MENU_LAYOUT: (state, layout) => (state.menuLayout = layout),
        MENU_OVER(state, top) {
          state.menuOverPosition = top
          state.menuOverDisplay = true
        },
        MENU_OVER_OUT(state) {
          state.menuOverDisplay = false
        },
        LOGIN(state, options) {
          state.user = options
          storage.setItem('auth', options.auth)
        },
        LOGOUT(state) {
          state.user = {}
          storage.removeItem('auth')
        },
        CHANNEL(state, options) {
          state.channel = Object.assign({}, options)
        }
      },
      actions: {
        setMenus(context, options) {
          context.commit('SET_MENUS', options)
        },
        setCurrentMainMenuIndex(context, options) {
          context.commit('SET_CURRENT_MAIN_MENU_INDEX', options)
        },
        menuClick(context, options) {
          if (options.hasOwnProperty('link')) {
            storage.setItem('menuEndClick', options.link)
          }
          switch (options.type) {
            case 'main':
              context.commit('SET_CURRENT_MAIN_MENU_INDEX', options.index)
              break
            case 'sub':
              context.commit('SET_SUB_MENU_INDEX', options.index)
              break
            case 'open':
              context.commit('SET_SUB_OPEN_INDEX', options)
              break
          }
        },
        switchMenuLayout(context, options) {
          context.commit('SWITCH_MENU_LAYOUT', options)
        },
        menuOver(context, options) {
          context.commit('MENU_OVER', options)
        },
        menuOverOut(context, options) {
          context.commit('MENU_OVER_OUT', options)
        },
        login(context, options) {
          context.commit('LOGIN', options)
        },
        logout(context) {
          context.commit('LOGOUT')
        },
        channel(context, options) {
          context.commit('CHANNEL', options)
        }
      }
    }
  }
});
export default store;
