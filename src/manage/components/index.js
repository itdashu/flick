import BatchHandle from "./BatchHandle";
import CustomCard from "./CustomCard";
import DataCard from "./DataCard";
import QuickSearch from "./QuickSearch";
import SearchForm from "./SearchForm";
import CustomFormItem from "./CustomFormItem";
import Description from "./Description";
import DataTable from "./DataTable";
import CustomLoading from "./CustomLoading";

export default {
  install(Vue) {
    Vue.component('batch-handle', BatchHandle)
    Vue.component('custom-card', CustomCard)
    Vue.component('data-card', DataCard)
    Vue.component('quick-search', QuickSearch)
    Vue.component('search-form', SearchForm)
    Vue.component('custom-form-item', CustomFormItem)
    Vue.component('description', Description)
    Vue.component('data-table', DataTable)
    Vue.component('custom-loading', CustomLoading)
  }
}