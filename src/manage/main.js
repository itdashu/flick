import Vue from 'vue'
import store from './config/store'
import router from './config/router'
import request from './config/request'
import App from "./App"
import ViewDesign from 'view-design'
import Components from './components/index'
import './style/global.less'
import './style/theme.less'

Vue.use(ViewDesign)
Vue.use(Components)
Vue.prototype.$axRequest = request

new Vue({
  el: '#app',
  store,
  router,
  render: h => h(App)
})