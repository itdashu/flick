import Test from '@/test/views/Test'

export default {
  routes: [
    {
      path: '/test',
      name: 'test',
      component: Test
    }
  ]
}